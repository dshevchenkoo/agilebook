import * as React from 'react'
import './App.css'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Front from '../Front/Front'

class App extends React.Component {

  public render() {
    return (
      <Router basename="/agilebook">
        <div className="App-router-wrapper">
          <Route exact={true} path="/" component={Front} />
        </div>
      </Router>
    );
  }
}

export default App
