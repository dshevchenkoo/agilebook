# Agilebook

A web-based Agile diary for personal development.

[React + Typescript](https://github.com/Microsoft/TypeScript-React-Starter#typescript-react-starter)-based project brought to you by [Dshevchenkoo](https://github.com/dshevchenkoo) and [x-doggy](https://x-doggy.gitlab.io).

