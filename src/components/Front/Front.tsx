import * as React from 'react'
import * as PropTypes from 'prop-types'
import { withStyles, WithStyles, createStyles } from '@material-ui/core/styles'
import './Front.css'

const styles = createStyles({
  root: {
    background: '#e7a6a6',
    borderRadius: 3,
    color: '#444',
    border: 0,
    "&:hover": {
      backgroundColor: "#ff5c5c"
    }
  },
  label: {
    textTransform: 'capitalize',
    textDecoration: 'none'
  }
})

interface ISpinnerProps {
  classes?: any;
}

class Front extends React.Component<ISpinnerProps & WithStyles<"root">, {}> {

  public static propTypes = {
    classes: PropTypes.object.isRequired
  }

  public constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <div className="FrontIndex">
        <div className="FrontIndex__body">
          <h1 className="FrontIndex__title">Agilebook</h1>
          <span className="FrontIndex__description">reopen for yourself yourself</span>
          <h2 className="FrontIndex__message">Coming soon</h2>
          <span className="FrontIndex__authors">by <a href="https://vk.com/dshevchenkoo" className="FrontIndex__link" target="_blank">Dshevchenkoo</a> and <a href="https://vk.com/x_doggy" className="FrontIndex__link" target="_blank">x-doggy</a></span>
        </div>
      </div>
    )
  }

}

export default withStyles(styles)(Front);